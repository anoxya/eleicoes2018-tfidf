from os import walk

import re

import math

from textblob import TextBlob as tb

def tf(word, blob):
    return blob.words.count(word) / len(blob.words)

def n_containing(word, bloblist):
    return sum(1 for blob in bloblist if word in blob.words)

def idf(word, bloblist):
    return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))

def tfidf(word, blob, bloblist):
    return tf(word, blob) * idf(word, bloblist)

files = [
	'txt/proposta_1534284632231.pdf.txt', # bolsonaro
	'txt/proposta_1535040841979.pdf.txt',
	'txt/proposta_1533576953009.pdf.txt',
	'txt/proposta_1533849607885.pdf.txt',
	'txt/proposta_1533774159360.pdf.txt',
	'txt/proposta_1534304719669.pdf.txt',
	'txt/proposta_1536702143353.pdf.txt', # haddad
	'txt/proposta_1534354939646.pdf.txt',
	'txt/proposta_1534522080782.pdf.txt',
	'txt/proposta_1533565462424.pdf.txt',
	'txt/proposta_1533938913830.pdf.txt',
	'txt/proposta_1534349279487.pdf.txt',
	'txt/proposta_1534450200223.pdf.txt'
]

data = []

for file in files:
    with open(file) as myfile:
        content = "".join(line.rstrip() for line in myfile)

        content = content.lower()

        content = re.sub(r"R[$]", " TOKENDINHEIRO ", content)
        content = re.sub(r"%", " TOKENPORCENTO ", content)
        content = re.sub(r"[0-9]+", " ", content)
        content = re.sub(r"[•●]", " ", content)
        content = re.sub(r"\x07", " ", content)

        datum = tb(content)
        data.append(datum)

bloblist = data

for i, blob in enumerate(bloblist):
    print(files[i])

    scores = {word: tfidf(word, blob, bloblist) for word in blob.words}

    sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)

    for word, score in sorted_words[:100]:
        print("\t{} ; {}".format(word, round(score, 5)))
